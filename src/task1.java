import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Locale;

public class task1 {

    public void MyAge (int yearOfBirth, int monthOfBirth,int dayOfBirth,
                       int hourOfBirth, int minOfBirth, int secOfBirth)
    {
        LocalDateTime birth = LocalDateTime.of(yearOfBirth,monthOfBirth,dayOfBirth,hourOfBirth,minOfBirth,secOfBirth);
        LocalDateTime now = LocalDateTime.now();
        System.out.println("Age at: ");
        System.out.println("year " + ChronoUnit.YEARS.between(birth, now));
        System.out.println("month " + ChronoUnit.MONTHS.between(birth, now));
        System.out.println("day " + ChronoUnit.DAYS.between(birth, now));
        System.out.println("hour " + ChronoUnit.HOURS.between(birth, now));
        System.out.println("minute " + ChronoUnit.MINUTES.between(birth, now));
        System.out.println("second " + ChronoUnit.SECONDS.between(birth, now));

    }

    public void BetweenDate(String firstDate, String secondDate)
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate ld1 = LocalDate.parse(firstDate,formatter);
        LocalDate ld2 = LocalDate.parse(secondDate,formatter);
        System.out.println("Days between " + Math.abs(ChronoUnit.DAYS.between(ld1,ld2)));
    }
    public void ConvertDate(String date)
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEEE, MMM dd, yyyy hh:mm:ss a", Locale.ENGLISH);
        LocalDate formatedDate = LocalDate.parse(date,formatter);
        System.out.println(formatedDate);
    }
}
